#include "apue.h"
#include "apue_db.h"
#include <fcntl.h>

void readData(FILE* fp, DBHANDLE db)
{
	char line[1024] = { 0 };
	char* key;
	char* value;
	int len = sizeof(line);

	while (!feof(fp)) {
		if (fgets(line, len, fp) != NULL) {
			key = strtok(line, ",");
			printf("key: %s ", value);
			value = strtok(NULL, ",");
			printf("value: %s ", value);
			if (db_store(db, key, value, DB_INSERT) != 0)
				err_quit("db_store error for %s", key);
		}
	}
}

int main(int argc, char* argv[])
{
	FILE *fp;
	DBHANDLE	db;

	if ((db = db_open("db4", O_RDWR | O_CREAT | O_TRUNC,
	  FILE_MODE)) == NULL)
		err_sys("db_open error");

	fp = fopen(argv[1], "r");
	if (fp == NULL)
		err_sys("file error");
	readData(fp, db);
	fclose(fp);

	db_close(db);
	exit(0);
}
